
public class Methoden {

	public static void main(String[] args) {
		System.out.println(umrechnung(10, "USD"));
	}
	
	public static double multiplikation(double zahl1, double zahl2) {
		double ergebnis = zahl1 * zahl2;
		return ergebnis;
	}
	
	public static double umrechnung(double budget, String waehrung) {
		double ergebnis = 0;
		if(waehrung == "USD" || waehrung == "usd") {
			return ergebnis = budget * 1.22;
		}else if(waehrung == "JPY" || waehrung == "jpy") {
			return ergebnis = budget * 126.50;
		} else if(waehrung == "GBP" || waehrung == "gbp") {
			return ergebnis = budget * 0.89;
		} else if(waehrung == "CHF" || waehrung == "chf") {
			return ergebnis = budget * 1.08;
		} else if(waehrung == "SEK" || waehrung == "sek") {
			return ergebnis = budget * 10.10;
		}
		return ergebnis;
	}
	
}
