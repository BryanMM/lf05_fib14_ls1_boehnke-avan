import java.util.Scanner;

public class Konsoleneingabe1 {
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Hallo, wie ist dein Name?");
		String name = tastatur.next();
		
		System.out.printf("Hallo %s! Darf ich wissen, wie alt du bist?", name);
		int age = tastatur.nextInt();
		
		System.out.println("Vielen Dank!");
		System.out.printf("Name: %s\nAlter: %s", name, age);
	}

	

}
