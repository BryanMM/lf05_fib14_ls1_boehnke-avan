import java.util.Scanner; // Import der Klasse Scanner 
 
public class Konsoleneingabe  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
    
    System.out.println("Welche Rechenart m�chten Sie verwenden?");
    System.out.println("Geben Sie f�r Plus '1', Minus '2', Mal '3' oder Geteilt '4' ein!");
    int rechenart = myScanner.nextInt();
    int ergebnis;
    if(rechenart == 1 || rechenart == 2 || rechenart == 3 || rechenart == 4) {
    	
    	switch((int) rechenart) {
    		case 1:
    			ergebnis = zahl1 + zahl2;
    			System.out.printf("Ergebnis (Plus): %s\n", ergebnis);
    			break;
    		case 2:
    			ergebnis = zahl1 - zahl2;
    			System.out.printf("Ergebnis (Minus): %s\n", ergebnis);
    			break;
    		case 4:
    			ergebnis = zahl1 / zahl2;
    			System.out.printf("Ergebnis (Geteilt): %s\n", ergebnis);
    			break;
    		case 3:
    			ergebnis = zahl1 * zahl2;
    			System.out.printf("Ergebnis (Mal): %s\n", ergebnis);
    			break;
    	}
    } else {
    	System.out.println("Error: Falsche Rechenart!");
    }
     
 
    myScanner.close(); 
     
  } 
}