
public class Konfigurationschaos {

	public static void main(String[] args) {
		// Aufgabe 1
		int cent;
		cent = 70;
		cent = 80;
		
		System.out.println(cent);
		
		// Aufgabe 2
		char ch = '#';
		int minus = -1000;
		double num = 1.255;
		boolean wahr = true;
		String sentence = "Helfen Sie mir.";
		final int con = 8765;
		
		
		/* Gr�nde, f�r Datentypen:
		*	1. Speichernutzung managen. Da die Speichertypen (int, double, String) unterschiedlich viel Speicher verbrauchen, ist es besser, sowas durch Datentypen zu managen
		*	2. Architektur: Einige Systeme k�nnten bestimmte Typen nicht verarbeiten aufgrund 8-bit Technik, etc.
		*/
		
		
		// Aufgabe 1 (Operatoren)
		int ergebnis = 4 + 8 * 9 - 1;
		System.out.println(ergebnis);
		int zaehler = 1;
		System.out.println(zaehler);
		ergebnis = 22 / 6;
		System.out.println(ergebnis);
		
		// Aufgabe 2
		int schalter = 10;
		if(schalter > 7 || schalter < 12) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		System.out.println(schalter);
		if(!(schalter == (10)) || schalter == 12) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		// Aufgabe 3
		System.out.println("Meine Oma " + "f�hr im " + "H�hnerstall Motorrad");
		
	}

}
