import java.util.Scanner;
public class Fahrkartenautomat_Methoden {

	public static void main(String[] args) {
		boolean wartung = false;
		double version = 1.6;
		
		while(true) {
	     if(wartung == true) {
	    	 System.out.println("                                        X X X \n                                        - - -\n   Hinweis: Der Automat befindet sich in Wartung, und ist zurzeit leider nicht Nutzbar.\n      Bitte weichen Sie auf die anderen, in dem Bahnhof befindlichen, Automaten aus.\n                                        X X X \n                                        - - -");
	    	 System.out.println(                                         "                                        +-+-+\r\n"
	    	 		+ "                                        |D|B|\r\n"
	    	 		+ "                                        +-+-+ \n              Deutsche Bahn AG x SmartRideAG - Ticketsoft2030 (c) 2021\n\n\n                                     Version: " + version + "\n\n                                 Entwickler: Bryan B.");
	    	 break;
	    	 
	     }
			System.out.printf("Ihr Restgeld betr�gt: %.2f", fahrkartenBezahlen(fahrkartenbestellungErfassen()));
			fahrkartenAusgeben();
		}

	}
	
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double ticketPreis = 1.50;
		if(ticketPreis < 0) {
			System.out.println("Fehler: Der Ticketpreis darf nicht negativ sein. Der Wert wurde auf '1,50' zurckgesetzt!");
			ticketPreis = 1.50;
		}
		int ticketNr;
		double zuZahlenderBetrag = 0;
		int ticketTyp = 0;
		while(ticketTyp == 0 || ticketPreis == 1.50) {
		System.out.println("Bitte w�hlen Sie Ihren Tickettyp aus!\nSie k�nnen w�hlen zwischen:\n");
		printTickets();
		System.out.println("Ihre Wahl:");
		ticketTyp = tastatur.nextInt();
		switch(ticketTyp) {
			case 1:
				ticketPreis = 2.90;
				break;
			case 2:
				ticketPreis = 3.30;
				break;
			case 3:
				ticketPreis = 3.60;
				break;
			case 4:
				ticketPreis = 1.90;
				break;
			case 5:
				ticketPreis = 8.60;
				break;
			case 6:
				ticketPreis = 9.0;
				break;
			case 7:
				ticketPreis = 9.60;
				break;
			case 8:
				ticketPreis = 23.50;
				break;
			case 9:
				ticketPreis = 24.30;
				break;
			case 10:
				ticketPreis = 24.90;
				break;
			default:
				ticketTyp = 0;
				ticketPreis = 1.50;
				System.out.println("Fehler: Sie haben ein ung�ltiges Ticket ausgew�hlt, bitte w�hlen Sie erneut.");
				
				
		}
		
		}
		System.out.println("Ticketpreis: " + ticketPreis + "�");
		System.out.println("Wie viele Tickets ben�tigen Sie? Geben Sie die Anzahl bitte ein!");
		ticketNr = tastatur.nextInt();
		if(ticketNr > 10) {
			System.out.println("Fehler: Sie k�nnen nicht mehr als 10 Tickets gleichzeitig kaufen. Der Wert wurde auf '1' gesetzt.");
			ticketNr = 1;
		}
		
		double arr = zuZahlenderBetrag = ticketPreis * ticketNr;
		
	
		return arr;
		
	}
	
	private static void printTickets() {
		String[] tickets = {
				"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin AB"
		};
		double[] ticketPreise = {
				2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90
		};
		
		for(int i = 0; i < tickets.length; i++) {
			int neuAuswahl = i+1;
			 System.out.println(tickets[i] + " " + ticketPreise[i] + "� (" + neuAuswahl + ")");
		
		}
		
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		double r�ckgabebetrag, eingezahlterGesamtbetrag = 0, zuZahlenderBetrag = zuZahlen, eingeworfeneM�nze;
		Scanner tastatur = new Scanner(System.in);
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	    	   if(eingeworfeneM�nze > 2) {
	    		   System.out.print("Fehler: Sie haben versucht in H�her als 2� zu bezahlen.\nMind. 5Ct, h�chstens 2 Euro");
	    		   eingeworfeneM�nze = tastatur.nextDouble();
	    		   if(eingeworfeneM�nze > 2) {
	    			   return eingeworfeneM�nze;
	    		   }
	    	   }
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0) {
	    	   r�ckgeldAusgeben(r�ckgabebetrag);
	    	   
	       }
	       return r�ckgabebetrag;
		
	}
	public static boolean r�ckgeldAusgeben(double r�ckgabebetrag) {

	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(2, "Euro"));
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(1, "Euro"));
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(50, "Cent"));
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(20, "Cent"));
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(10, "Cent"));
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(5, "Cent"));
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }
	       return true;
	       
	}
	public static void fahrkartenAusgeben() {

			System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          warte(250);
		       }
		       System.out.println("\n\n");

		}
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static String muenzeAusgabe(int betrag, String einheit) {
		warte(200);
		String s = betrag + " " + einheit;
		return s;
		
		
	}
}


