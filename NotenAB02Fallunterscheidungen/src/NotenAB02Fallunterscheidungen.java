import java.io.IOException;
import java.util.Scanner;
public class NotenAB02Fallunterscheidungen {

	public static void main(String[] args) {
		
		while(1 == 1) {
			int note = 0;
			System.out.println("Bitte geben Sie Ihre Note ein! (Bitte geben Sie nur volle Zahlen an!)");
			Scanner tastatur = new Scanner(System.in);
			note = tastatur.nextInt(); 
			System.out.println(noteBerechnen(note));
		}
		

	}
	
	public static String noteBerechnen(int note) {
		switch(note) {
		case 1:
			return "Sehr gut";
		case 2: 
			return "Gut";
		case 3:
			return "Befriedigend";
		case 4:
			return "Ausreichend";
		case 5:
			return "Mangelhaft";
		case 6:
			return "Ungenügend";
		default:
			return "Den von Ihnen eingegebener Wert wird nicht unterstützt. Bitte geben Sie eine Zahl von 1 bis 6 ein!";
		}
		
	}

}
