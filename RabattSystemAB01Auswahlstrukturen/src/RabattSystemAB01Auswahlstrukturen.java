import java.util.Scanner;

public class RabattSystemAB01Auswahlstrukturen {

	public static void main(String[] args) {
		double bestellwert = 0; // Gesamtwert der Bestellung
		double zahlenderBetrag = 0; // Endbetrag, der vom Kunden zu bezahlen ist
		double rabattBetrag = 0; // Endbetrag mit Rabatt abgezogen (exkl. MwSt.)
		int mwst = 19; // Mehrwertsteuer
		int rabatt = 0; // Rabatt in Prozent
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie Ihren Bestellwert an!");
		bestellwert = tastatur.nextDouble();
		
		if(bestellwert < 100 && bestellwert > 0) { // Wenn BW unter 100, aber �ber 0, dann 10% Rabatt
			rabatt = 10;
		} else if(bestellwert > 100 && bestellwert < 500) { // Wenn BW �ber 100, aber unter 500, dann 15% Rabatt
			rabatt = 15;
		} else if(bestellwert > 500) { // Wenn BW �ber 500, dann 20% Rabatt
			rabatt = 20;
		}

		rabattBetrag = bestellwert - (bestellwert / 100 * rabatt); //Rabatt dem Endbetrag gegenrechnen
		zahlenderBetrag = rabattBetrag + (rabattBetrag / 100 * mwst); //Endbetrag errechnen
		System.out.println("Ihr zu zahlender Betrag liegt bei " + zahlenderBetrag + "�. Ihnen wurde ein Rabatt von " + rabatt + "% gew�hrt.");
	}

}
