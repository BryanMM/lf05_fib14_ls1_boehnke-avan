public class Konsolenausgabe {
	public static void main(String[] args) {
		
		/*
		 * println schreibt den eingegebenen Text in die Konsole und macht automatisch einen Zeilenumbruch "\n"
		 * print schreibt den eingegebenen Text in die Konsole ohne Zeilenumbruch - Weitere prints
		 * 
		 */
		
		// Aufgabe 1
		System.out.println("Das ist ein \"Beispielsatz\"!");
		System.out.print("Ein Beispielsatz ist das \n");
		String name = "Hr. Zickermann";
		System.out.println("Hi " + name);
		
		// Aufgabe 2
		double number1 = 22.4234234;
		double number2 = 111.2222;
		double number3 = 4.0;
		double number4 = 1000000.551;
		double number5 = 97.34;
		
	    System.out.println("        *"); 
	    System.out.println("       ***");
	    System.out.println("      *****");
	    System.out.println("     *******");
	    System.out.println("    *********");
	    System.out.println("   ***********");
	    System.out.println("  *************");
	    System.out.println("       ***");
	    System.out.println("       ***" + "\n\n");
	    
	    // Aufgabe 3
	    System.out.printf("%.2f\n", number1);
	    System.out.printf("%.2f\n", number2);
	    System.out.printf("%.2f\n", number3);
	    System.out.printf("%.2f\n", number4);
	    System.out.printf("%.2f\n", number5);
	    
	}
}