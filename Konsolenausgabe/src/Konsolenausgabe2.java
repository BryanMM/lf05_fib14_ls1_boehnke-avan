class Konsolenausgabe2 {
	public static void main(String[] args) {
		
		// Aufgabe ohne klassisches Leerzeichen - Leerzeichen maskiert | Tab w�re auch m�glich gewesen, dann sehe es aber nicht so toll aus, wie in der Aufgabenbeschreibung mit dem Beispiel
		
		// Aufgabe 1
		System.out.println("\s\s\s\s**");
		System.out.println("*\t\s*\n*\t\s*");
		System.out.println("\s\s\s\s**\n\n");
		
		// Aufgabe 2
		System.out.printf("%-5s= %1s %18s %1s\n", 	"0!","","=",1);
		System.out.printf("%-5s= %1s %18s %1s\n", 	"1!","1","=",1);
		System.out.printf("%-5s= %1s %14s %1s\n", 	"2!","1 * 2","=",2);
		System.out.printf("%-5s= %1s %10s %1s\n", 	"3!","1 * 2 * 3","=",6);
		System.out.printf("%-5s= %1s %6s %1s\n", 	"4!","1 * 2 * 3 * 4","=",24);
		System.out.printf("%-5s= %1s %2s %1s\n\n", 	"5!","1 * 2 * 3 * 4 * 5","=",120);
		
		//Aufgabe 3
		
		System.out.printf("Fahrenheit %3s %4s Celsius\n", "|", "");
		System.out.printf("---------------------------\n");
		System.out.printf("-20 %10s %12s\n", "|", -28.89);
		System.out.printf("-10 %10s %12s\n", "|", -23.33);
		System.out.printf("+0 %11s %12s\n", "|", -17.78);
		System.out.printf("+20 %10s %12s\n",  "|", -6.67);
		System.out.printf("+30 %10s %12s\n", "|", -1.11);
	}
}